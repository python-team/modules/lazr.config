# Work around difficulties with pre-PEP-420 namespace packages: the .pth
# files from build-dependencies are loaded during site initialization,
# adding the system "lazr" package directory to sys.path, but we want to
# ensure that subpackages of lazr can be imported both from our
# build-dependencies and from this package while running tests.
#
# This file allows "sphinx-build -c debian" to import lazr.config.

import os.path

import lazr

top = os.path.dirname(os.path.dirname(__file__))
lazr.__path__[:0] = [os.path.join(top, "src", "lazr")]
with open(os.path.join(top, "src", "lazr", "config", "docs", "conf.py")) as f:
    exec(f.read())
