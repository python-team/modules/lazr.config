lazr.config (3.1-1) unstable; urgency=medium

  * Team Upload
  * New upstream version 3.1
  * Apply Multi-Arch: hint
  * Use dh-sequence-python3
  * Update standards version to 4.7.0, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Thu, 06 Feb 2025 07:08:04 +0100

lazr.config (3.0-1) unstable; urgency=medium

  * Update standards version to 4.6.2, no changes needed.
  * Update watch file format version to 4.
  * Opportunistically check upstream PGP signatures.
  * Use sphinx-build rather than "setup.py build_sphinx" (closes: #1042640).
  * New upstream release.
  * Add Built-Using to python-lazr.config-doc.

 -- Colin Watson <cjwatson@debian.org>  Sun, 19 Nov 2023 22:26:02 +0000

lazr.config (2.2.3-3) unstable; urgency=medium

  * Switch from nose to pytest (closes: #1018390).
  * Work around pre-PEP-420 namespace package path issues (closes: #866962).
  * Add myself to Uploaders.

 -- Colin Watson <cjwatson@debian.org>  Sat, 10 Dec 2022 00:06:31 +0000

lazr.config (2.2.3-2) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 25 May 2022 20:51:59 +0100

lazr.config (2.2.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Tue, 26 Jan 2021 09:59:34 +0000

lazr.config (2.2.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jonas Meurer ]
  * New upstream release. (Closes: #971093)
  * d/control:
    - Add myself to Uploaders
    - Remove Barry Warsaw from Uploaders
    - Update standards version to 4.5.1, no changes needed.
  * Disable test `test_not_stackable` which fails for python3.9
    (Closes: #970148)

 -- Jonas Meurer <jonas@freesources.org>  Sat, 02 Jan 2021 00:44:47 +0100

lazr.config (2.2-2) unstable; urgency=medium

  [ Barry Warsaw ]
  * d/control: Put DPMT in Maintainers and myself in Uploaders.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #936820
  * debian/control
    - bump Standards-Version to 4.4.1 (no changes needed)
    - dedup short descriptions

 -- Sandro Tosi <morph@debian.org>  Sat, 26 Oct 2019 12:44:57 -0400

lazr.config (2.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Barry Warsaw ]
  * New upstream release.
  * d/control:
    - Bump Standards-Version to 4.0.0 with no other changes needed.
    - Remove now-redundant XS-Testsuite header.

 -- Barry Warsaw <barry@debian.org>  Thu, 29 Jun 2017 09:43:46 -0400

lazr.config (2.1-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bumped Standards-Version to 3.9.6 with no other changes needed.
  * d/rules:
    - Added PYBUILD_TEST_NOSE=1 to invoke the correct test runner.
    - Add override_dh_sphinxdoc as a hack to prevent .pybuild directories
      from leaking into the python-lazr.config-doc package.  (Closes: #786516)
  * d/README.source: Fix typo.
  * d/copyright:
    - Fix typo.
    - Reorganize sections to keep lintian happy.
  * d/watch: Use the new pypi.debian.net redirector url.

 -- Barry Warsaw <barry@debian.org>  Fri, 05 Jun 2015 11:04:53 -0400

lazr.config (2.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #758827)

 -- Barry Warsaw <barry@debian.org>  Fri, 22 Aug 2014 17:41:44 -0400
